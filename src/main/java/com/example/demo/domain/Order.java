package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long Id;

    @Column(name = "order_date", updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDateTime orderDate;

    @Column(name = "dishes_date", updatable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDateTime dishesDate;

    @OneToMany
    @JoinTable( name="dishes",
            joinColumns = @JoinColumn( name="dishes_id"),
            inverseJoinColumns = @JoinColumn( name="id")
    )
    private List<Dish> dishes;

    @OneToOne
    @JoinTable( name="meals",
            joinColumns = @JoinColumn( name="meals_id"),
            inverseJoinColumns = @JoinColumn( name="id")
    )
    private Meal meals;

    @ManyToOne
    @JoinTable( name="users",
            joinColumns = @JoinColumn( name="users_id"),
            inverseJoinColumns = @JoinColumn( name="id")
    )
    private User users;

    public Order() { }

    public Order(Long Id, LocalDateTime orderDate, LocalDateTime dishesDate, List<Dish> dishes, Meal meals, User users) {
        this.Id = Id;
        this.orderDate = orderDate;
        this.dishesDate = dishesDate;
        this.dishes = dishes;
        this.meals = meals;
        this.users = users;
    }

    public Long getId() { return Id; }
    public void setId(Long Id) { this.Id = Id; }

    public LocalDateTime getOrderDate() { return orderDate; }
    public void setOrderDate(LocalDateTime orderDate) { this.orderDate = orderDate; }

    public LocalDateTime getDishesDate() { return dishesDate; }
    public void setDishesDate(LocalDateTime dishesDate) { this.dishesDate = dishesDate; }

    public List<Dish> getDishes() { return dishes; }
    public void setDishes(List<Dish> dishes) { this.dishes = dishes; }

    public Meal getMeals() { return meals; }
    public void setMeals(Meal meals) { this.meals = meals; }

    public User getUsers() { return users; }
    public void setUsers(User users) { this.users = users; }
}
